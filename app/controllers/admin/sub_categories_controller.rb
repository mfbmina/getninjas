class Admin::SubCategoriesController < ApplicationController
  before_filter :find_sub_category, only: [:edit, :update, :destroy]

  layout "admin"

  def index
    @sub_categories = SubCategory.all
  end

  def new
    @sub_category = SubCategory.new
  end

  def create
    @sub_category = SubCategory.new(sub_category_params)
    if @sub_category.save
      flash[:success] = 'Subcategoria criada com sucesso!'
      redirect_to admin_sub_categories_path
    else
      flash[:danger] = 'Erro ao criar subcategoria!'
      render :new
    end
  end

  def edit
  end

  def update
    if @sub_category.update_attributes(sub_category_params)
      flash[:success] = 'Subcategoria atualizada com sucesso!'
      redirect_to admin_sub_categories_path
    else
      flash[:danger] = 'Erro ao atualizar subcategoria!'
      render :edit
    end
  end

  def destroy
    if @sub_category.destroy
      flash[:success] = 'Subcategoria apagada com sucesso!'
    else
      flash[:danger] = 'Erro ao apagar subcategoria!'
    end
    redirect_to admin_sub_categories_path
  end

  private

  def sub_category_params
    params.require(:sub_category).permit(:category_id, :name, :slug, fields_attributes: field_params)
  end

  def field_params
    [:id, :order, :title, :type, :_destroy, options_attributes: options_params]
  end

  def options_params
    [:id, :title, :value, :_destroy]
  end

  def find_sub_category
    @sub_category = SubCategory.find(params[:id])
  end
end
