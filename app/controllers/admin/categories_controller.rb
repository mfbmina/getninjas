class Admin::CategoriesController < ApplicationController
  before_filter :find_category, only: [:edit, :update, :destroy]

  layout 'admin'

  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = 'Categoria criada com sucesso!'
      redirect_to admin_categories_path
    else
      flash[:danger] = 'Erro ao criar categoria!'
      render :new
    end
  end

  def edit
  end

  def update
    if @category.update_attributes(category_params)
      flash[:success] = 'Categoria atualizada com sucesso!'
      redirect_to admin_categories_path
    else
      flash[:danger] = 'Erro ao atualizar categoria!'
      render :edit
    end
  end

  def destroy
    if @category.destroy
      flash[:success] = 'Categoria apagada com sucesso!'
    else
      flash[:danger] = 'Erro ao apagar categoria!'
    end
    redirect_to admin_categories_path
  end

  private

  def category_params
    params.require(:category).permit(:name, :slug)
  end

  def find_category
    @category = Category.find(params[:id])
  end
end
