class SubCategoriesController < ApplicationController
  def show
    @sub_category = SubCategory.where(slug: params[:sub_category_slug]).categories_by_slug(params[:category_slug]).first
  end
end
