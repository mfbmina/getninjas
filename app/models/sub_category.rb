class SubCategory < ActiveRecord::Base
  belongs_to :category
  has_many :fields, dependent: :destroy

  accepts_nested_attributes_for :fields, allow_destroy: true

  scope :categories_by_slug, -> (slug) { joins(:category).where(categories: { slug: slug }) }

  validates :name, :slug, :category, presence: true
end
