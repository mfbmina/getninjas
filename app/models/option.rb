class Option < ActiveRecord::Base
  belongs_to :optionable, polymorphic: true

  validates :title, :value, presence: true
end
