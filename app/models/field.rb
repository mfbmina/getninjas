class Field < ActiveRecord::Base
  TYPES = %w(checkbox select text textarea)

  belongs_to :sub_category
  has_many :options, as: :optionable

  accepts_nested_attributes_for :options, allow_destroy: true, reject_if: :without_option?

  validates :order, :title, :type, presence: true

  scope :by_order_value, -> { order(:order) }

  private

  def without_option?
    type == 'Text' || type == 'Textarea'
  end
end
