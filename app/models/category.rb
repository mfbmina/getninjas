class Category < ActiveRecord::Base
  has_many :sub_categories, dependent: :destroy

  validates :name, :slug, presence: true
end
