module ApplicationHelper
  def categories_for_select
    Category.pluck(:name, :id)
  end

  def field_types_for_select
    Field::TYPES.map { |t| [t, t.capitalize] }
  end

  def fields_amount_for_select(id)
    size = Field.where(sub_category_id: id).size
    1..size + 1
  end

  def link_to_remove_associations(name, f, association)
    f.hidden_field(:_destroy) + link_to(name, '#', onclick: "remove_associations(this, \"#{association}\")")
  end

  def link_to_add_associations(name, f, association)
    return unless f.object.respond_to?(association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render("#{association}_form", f: builder)
    end
    link_to(name, '#', onclick: "add_associations(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end

  def checkbox_builder(field)
    response = field.options.inject('') do |list, option|
      list += "<div class='checkbox'><label>#{check_box_tag(option.title, option.value)} #{option.title}</label></div>"
    end
    response.html_safe
  end

  def text_builder(field)
    text_field_tag field.title, nil, class: "form-control"
  end

  def select_builder(field)
    options = field.options.pluck(:title, :value)
    select_tag(field.title, options_for_select(options), {class: "form-control"})
  end

  def textarea_builder(field)
    text_area_tag(field.title, nil, class: "form-control", rows: 4)
  end
end
