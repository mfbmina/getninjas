module FlashMessageHelper
  def flash_message
    messages = ""
    [:success, :danger].each do |type|
      if flash[type]
        messages += "<div class=\"alert alert-#{type}\"><strong>#{flash[type]}.</div>"
      end
    end
    messages.html_safe
  end
end
