// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap-sprockets
//= require jquery_ujs
//= require_tree .

function remove_associations(link, association) {
  $(link).prev("input[type=hidden]").val("1");
  debugger
  $(link).closest("." + association).hide();
}

function add_associations(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).parent().before(content.replace(regexp, new_id));
}

function show_or_hide_options(object) {
  if((object.value === 'Text') || (object.value === 'Textarea')) {
    $(object).closest('.panel-body').find('.options-box').first().hide();
  }
  else {
    $(object).closest('.panel-body').find('.options-box').first().show();
  }
}

$(document).ready(function(){
  if($('#sub_menu').length > 0) {
    var anchor_offset = $('.breadcrumb').offset().top;

    $(window).on('scroll', function() {
      if($(window).scrollTop() > anchor_offset) {
        $('#sub_menu').addClass('hidden');
      }
      else {
        $('#sub_menu').removeClass('hidden');
      }
    });
  }
});
