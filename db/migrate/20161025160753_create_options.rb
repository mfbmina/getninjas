class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.text :title
      t.text :value
      t.references :optionable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
