class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.integer :order
      t.string :title
      t.string :type
      t.references :sub_category, index: true

      t.timestamps null: false
    end
  end
end
