require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#categories_for_select' do
    it 'should return a matrix with category name and category id' do
      category = create(:category)
      expect(categories_for_select).to eq([[category.name, category.id]])
    end
  end

  describe '#field_types_for_select' do
    it 'should return a matrix with fielf types' do
      duck_response = [%w(checkbox Checkbox), %w(select Select), %w(text Text), %w(textarea Textarea)]
      expect(field_types_for_select).to eq(duck_response)
    end
  end

  describe '#fields_amount_for_select' do
    it 'should return a range between 1 and sub_category fields size plus one' do
      field = create(:field)
      expect(fields_amount_for_select(field.sub_category_id)).to eq(1..2)
    end
  end

  describe '#link_to_remove_associations' do
    let(:f) { double }
    let(:association) { :fields }
    let(:name) { 'Remover' }

    before :each do
      allow(f).to receive(:hidden_field).and_return('hidden_field_mock')
    end

    it 'should return a hidden field' do
      expect(link_to_remove_associations(name, f, association)).to include('hidden_field_mock')
    end

    it 'should return a link_to' do
      duck_response = "<a onclick=\"remove_associations(this, &quot;#{association}&quot;)\" href=\"#\">#{name}</a>"
      expect(link_to_remove_associations(name, f, association)).to include(duck_response)
    end
  end

  describe '#link_to_add_associations' do
    let(:f) { double }
    let(:association) { :fields }
    let(:name) { 'Adicionar' }

    before :each do
      allow(f).to receive(:object).and_return(build(:sub_category))
      allow(f).to receive(:fields_for).and_return('')
    end

    it 'should return a link_to' do
      duck_response = "<a onclick=\"add_associations(this, &quot;#{association}&quot;, &quot;&quot;)\" href=\"#\">#{name}</a>"
      expect(link_to_add_associations(name, f, association)).to eq(duck_response)
    end
  end

  describe '#checkbox_builder' do
    let(:option) { create(:option) }

    it 'should return a checkbox list with all options' do
      duck_response = "<div class='checkbox'><label><input type=\"checkbox\" name=\"Option 1\" id=\"Option_1\" value=\"1\" /> Option 1</label></div>"
      expect(checkbox_builder(option.optionable)).to eq(duck_response)
    end
  end

  describe '#select_builder' do
    let(:option) { create(:option) }

    it 'should return a select list with all options' do
      duck_response = "<select name=\"nome\" id=\"nome\" class=\"form-control\"><option value=\"1\">Option 1</option></select>"
      expect(select_builder(option.optionable)).to eq(duck_response)
    end
  end

  describe '#text_builder' do
    let(:field) { create(:field) }

    it 'should return a text field' do
      duck_response = "<input type=\"text\" name=\"nome\" id=\"nome\" class=\"form-control\" />"
      expect(text_builder(field)).to eq(duck_response)
    end
  end

  describe '#textarea_builder' do
    let(:field) { create(:field) }

    it 'should return a checkbox list with all options' do
      duck_response = "<textarea name=\"nome\" id=\"nome\" class=\"form-control\" rows=\"4\">\n</textarea>"
      expect(textarea_builder(field)).to eq(duck_response)
    end
  end
end
