FactoryGirl.define do
  factory :sub_category do
    name "Artes"
    slug  "artes"
    category
  end
end
