FactoryGirl.define do
  factory :option do
    title 'Option 1'
    value '1'
    association :optionable, factory: :checkbox
  end
end
