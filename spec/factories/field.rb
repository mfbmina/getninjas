FactoryGirl.define do
  factory :field do
    order '1'
    title 'nome'
    type 'Text'
    sub_category
  end

  factory :checkbox, class: Checkbox do
    order '1'
    title 'nome'
    type 'Checkbox'
    sub_category
  end
end
