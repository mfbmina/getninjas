describe "category listing page", type: :feature do
  it "shows me all existing categories" do
    create(:category)
    visit '/admin/categories'
    expect(page).to have_content 'Artes'
    expect(page).to have_content 'artes'
  end
end

describe "new category", type: :feature do
  it "user can create a category filling all fields" do
    visit '/admin/categories'
    click_link 'Nova Categoria'
    fill_in 'Name', with: 'Jardinagem'
    fill_in 'Slug', with: 'jardinagem'
    click_button 'Salvar'
    expect(page).to have_content 'Categoria criada com sucesso!'
    expect(page).to_not have_content 'Erro ao criar categoria!'
  end

  it "category is not persisted if there are any blank fields" do
    visit '/admin/categories'
    click_link 'Nova Categoria'
    fill_in 'Name', with: 'Jardinagem'
    click_button 'Salvar'
    expect(page).to_not have_content 'Categoria criada com sucesso!'
    expect(page).to have_content 'Erro ao criar categoria!'
  end
end

describe "edit category", type: :feature do
  it "user can change an attribute" do
    create(:category)
    visit '/admin/categories'
    click_link 'Editar'
    fill_in 'Name', with: 'Jardinagem'
    fill_in 'Slug', with: 'jardinagem'
    click_button 'Salvar'
    expect(page).to have_content 'jardinagem'
    expect(page).to_not have_content 'artes'
    expect(page).to have_content 'Categoria atualizada com sucesso!'
    expect(page).to_not have_content 'Erro ao atualizar categoria!'
  end

  it "category is not persisted if there are any blank fields" do
    create(:category)
    visit '/admin/categories'
    click_link 'Editar'
    fill_in 'Slug', with: ''
    click_button 'Salvar'
    expect(page).to_not have_content 'Categoria atualizada com sucesso!'
    expect(page).to have_content 'Erro ao atualizar categoria!'
  end
end

describe "delete category", type: :feature, js: true do
  it "user can delete a category" do
    create(:category)
    visit '/admin/categories'
    page.accept_confirm do
      click_link 'Apagar'
    end
    expect(page).to have_content 'Categoria apagada com sucesso!'
    expect(page).to_not have_content 'Erro ao apagar categoria!'
    expect(page).to_not have_content 'artes'
  end

  it "category is not persisted if there are any blank fields" do
    create(:category)
    visit '/admin/categories'
    page.dismiss_confirm do
      click_link 'Apagar'
    end
    expect(page).to_not have_content 'Categoria apagada com sucesso!'
    expect(page).to have_content 'artes'
  end
end
