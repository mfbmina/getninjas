describe "subcategory listing page", type: :feature do
  it "shows me all existing sub_categories" do
    create(:sub_category)
    visit '/admin/sub_categories'
    expect(page).to have_content 'Artes'
    expect(page).to have_content 'artes'
  end
end

describe "new subcategory", type: :feature, js: true do
  before :each do
    create(:category)
  end

  it "user can create a subcategory filling all fields" do
    visit '/admin/sub_categories'
    click_link 'Nova Subcategoria'
    fill_in 'Name', with: 'Jardinagem'
    fill_in 'Slug', with: 'jardinagem'
    click_button 'Salvar'
    expect(page).to have_content 'Subcategoria criada com sucesso!'
    expect(page).to_not have_content 'Erro ao criar subcategoria!'
  end

  it "subcategory is not persisted if there are any blank fields" do
    visit '/admin/sub_categories'
    click_link 'Nova Subcategoria'
    fill_in 'Name', with: 'Jardinagem'
    click_button 'Salvar'
    expect(page).to_not have_content 'Subcategoria criada com sucesso!'
    expect(page).to have_content 'Erro ao criar subcategoria!'
  end

  it "can add new fields and options for these fields" do
    visit '/admin/sub_categories'
    click_link 'Nova Subcategoria'
    fill_in 'Name', with: 'Jardinagem'
    fill_in 'Slug', with: 'jardinagem'
    click_link 'Adicionar campo'
    fill_in 'Title', with: 'text 1'
    select('1', from: 'Order')
    select('text', from: 'Type')
    click_button 'Salvar'
    expect(page).to have_content 'Subcategoria criada com sucesso!'
    expect(page).to_not have_content 'Erro ao criar subcategoria!'
    click_link 'Visualizar formulário'
    expect(page).to have_field('text 1')
    expect(page).to have_button('Continuar')
  end
end

describe "edit subcategory", type: :feature, js: true do
  it "user can change an attribute" do
    create(:option)
    visit '/admin/sub_categories'
    click_link 'Editar'
    fill_in 'Name', with: 'Jardinagem'
    fill_in 'Slug', with: 'jardinagem'
    click_button 'Salvar'
    expect(page).to have_content 'jardinagem'
    expect(page).to_not have_content 'artes'
    expect(page).to have_content 'Subcategoria atualizada com sucesso!'
    expect(page).to_not have_content 'Erro ao atualizar subcategoria!'
  end

  it "subcategory is not persisted if there are any blank fields" do
    create(:option)
    visit '/admin/sub_categories'
    click_link 'Editar'
    fill_in 'Slug', with: ''
    click_button 'Salvar'
    expect(page).to_not have_content 'Subcategoria atualizada com sucesso!'
    expect(page).to have_content 'Erro ao atualizar subcategoria!'
  end
end

describe "delete subcategory", type: :feature, js: true do
  it "user can delete a subcategory" do
    create(:sub_category)
    visit '/admin/sub_categories'
    page.accept_confirm do
      click_link 'Apagar'
    end
    expect(page).to have_content 'Subcategoria apagada com sucesso!'
    expect(page).to_not have_content 'Erro ao apagar categoria!'
    expect(page).to_not have_content 'artes'
  end

  it "subcategory is not persisted if there are any blank fields" do
    create(:sub_category)
    visit '/admin/sub_categories'
    page.dismiss_confirm do
      click_link 'Apagar'
    end
    expect(page).to_not have_content 'Subcategoria apagada com sucesso!'
    expect(page).to have_content 'artes'
  end
end
