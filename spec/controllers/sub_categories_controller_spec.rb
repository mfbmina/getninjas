require 'rails_helper'

RSpec.describe SubCategoriesController, type: :controller do
  let(:sub_category) { create(:sub_category) }

  describe 'GET show' do
    it 'should initalize @sub_category' do
      get :show, sub_category_slug: sub_category.slug, category_slug: sub_category.category.slug
      expect(assigns(:sub_category)).to eq(sub_category)
    end

    it 'should render :show' do
      get :show, sub_category_slug: sub_category.slug, category_slug: sub_category.category.slug
      expect(response).to render_template('show')
    end
  end
end
