require 'rails_helper'

RSpec.describe Admin::CategoriesController, type: :controller do
  let(:category) { create(:category) }

  describe 'GET index' do
    it 'should initalize @categories' do
      get :index
      expect(assigns(:categories)).to eq([category])
    end

    it 'should render :new' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'GET new' do
    it 'should initalize @category' do
      get :new
      expect(assigns(:category)).to be_a_new(Category)
    end

    it 'should render :new' do
      get :new
      expect(response).to render_template('new')
    end
  end

  describe 'GET edit' do
    it 'should initalize @category' do
      get :edit, id: category.id
      expect(assigns(:category)).to eq(category)
    end

    it 'should render :edit' do
      get :edit, id: category.id
      expect(response).to render_template('edit')
    end
  end

  describe 'POST create' do
    context 'with valid attributes' do
      it 'should initalize @category' do
        post :create, category: attributes_for(:category)
        expect(assigns(:category)).to_not be_a_new(Category)
      end

      it 'should redirect_to :index' do
        post :create, category: attributes_for(:category)
        expect(response).to redirect_to(admin_categories_url)
      end
    end

    context 'with invalid attributes' do
      it 'should initalize @category' do
        post :create, category: { name: 'Jardinagem', slug: nil }
        expect(assigns(:category)).to be_a_new(Category)
      end

      it 'should render :new' do
        post :create, category: { name: 'Jardinagem', slug: nil }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PATCH update' do
    context 'with valid attributes' do
      it 'should change @category attributes' do
        patch :update, id: category.id, category: { name: 'Jardinagem' }
        expect(assigns(:category).name).to eq('Jardinagem')
      end

      it 'should redirect_to :index' do
        patch :update, id: category.id, category: { name: 'Jardinagem' }
        expect(response).to redirect_to(admin_categories_url)
      end
    end

    context 'with invalid attributes' do
      it 'should not change @category attributes' do
        patch :update, id: category.id, category: { name: nil }
        expect(assigns(:category).name).to_not eq('Jardinagem')
      end

      it 'should render :edit' do
        patch :update, id: category.id, category: { name: nil }
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE destroy' do
    it 'should delete @category and redirect_to :index' do
      delete :destroy, id: category.id
      expect(assigns(:category).persisted?).to be_falsey
      expect(response).to redirect_to(admin_categories_url)
    end
  end
end
