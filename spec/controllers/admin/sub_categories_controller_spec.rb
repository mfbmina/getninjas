require 'rails_helper'

RSpec.describe Admin::SubCategoriesController, type: :controller do
  let(:sub_category) { create(:sub_category) }

  describe 'GET index' do
    it 'should initalize @sub_categories' do
      get :index
      expect(assigns(:sub_categories)).to eq([sub_category])
    end

    it 'should render :new' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'GET new' do
    it 'should initalize @sub_category' do
      get :new
      expect(assigns(:sub_category)).to be_a_new(SubCategory)
    end

    it 'should render :new' do
      get :new
      expect(response).to render_template('new')
    end
  end

  describe 'GET edit' do
    it 'should initalize @sub_category' do
      get :edit, id: sub_category.id
      expect(assigns(:sub_category)).to eq(sub_category)
    end

    it 'should render :edit' do
      get :edit, id: sub_category.id
      expect(response).to render_template('edit')
    end
  end

  describe 'POST create' do
    context 'with valid attributes' do
      it 'should initalize @sub_category' do
        post :create, sub_category: attributes_for(:sub_category).merge(category_id: sub_category.category_id)
        expect(assigns(:sub_category)).to_not be_a_new(SubCategory)
      end

      it 'should redirect_to :index' do
        post :create, sub_category: attributes_for(:sub_category).merge(category_id: sub_category.category_id)
        expect(response).to redirect_to(admin_sub_categories_url)
      end
    end

    context 'with invalid attributes' do
      it 'should initalize @sub_category' do
        post :create, sub_category: { name: 'Jardinagem', slug: nil }
        expect(assigns(:sub_category)).to be_a_new(SubCategory)
      end

      it 'should render :new' do
        post :create, sub_category: { name: 'Jardinagem', slug: nil }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PATCH update' do
    context 'with valid attributes' do
      it 'should change @sub_category attributes' do
        patch :update, id: sub_category.id, sub_category: { name: 'Jardinagem' }
        expect(assigns(:sub_category).name).to eq('Jardinagem')
      end

      it 'should redirect_to :index' do
        patch :update, id: sub_category.id, sub_category: { name: 'Jardinagem' }
        expect(response).to redirect_to(admin_sub_categories_url)
      end
    end

    context 'with invalid attributes' do
      it 'should not change @sub_category attributes' do
        patch :update, id: sub_category.id, sub_category: { name: nil }
        expect(assigns(:sub_category).name).to_not eq('Jardinagem')
      end

      it 'should render :edit' do
        patch :update, id: sub_category.id, sub_category: { name: nil }
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE destroy' do
    it 'should delete @sub_category and redirect_to :index' do
      delete :destroy, id: sub_category.id
      expect(assigns(:sub_category).persisted?).to be_falsey
      expect(response).to redirect_to(admin_sub_categories_url)
    end
  end
end
