Rails.application.routes.draw do
  namespace :admin do
    resources :categories, except: :show
    resources :sub_categories, except: :show
  end

  get '/:category_slug/:sub_category_slug/', to: 'sub_categories#show', as: 'sub_category'

  root 'admin/categories#index'
end
